﻿using Microsoft.Azure.Cosmos;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace TestCosmosDbConn
{
    public class CustomHttpHandler : DelegatingHandler
    {
        public CustomHttpHandler(HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
        }

        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Console.WriteLine($"HTTP {request.Version} {request.Method} {request.RequestUri}");
            foreach (var header in request.Headers)
            {
                if (header.Value == null) continue;

                Console.Write($"{header.Key}: ");

                foreach (var headerValue in header.Value)
                {
                    Console.Write($"{headerValue} ");
                }
                Console.WriteLine("");
            }
            if (request.Content != null)
            {
                Console.WriteLine("");
                Console.WriteLine(request.Content);
            }
            Console.WriteLine("");
            Console.WriteLine("");

            var result = await base.SendAsync(request, cancellationToken);

            Console.WriteLine($"{(int) result.StatusCode} {result.ReasonPhrase}");
            foreach (var header in result.Headers)
            {
                if (header.Value == null) continue;

                Console.Write($"{header.Key}: ");

                foreach (var headerValue in header.Value)
                {
                    Console.Write($"{headerValue} ");
                }
                Console.WriteLine("");
            }

            var bodyRsp = await result.Content.ReadAsStringAsync();
            if (bodyRsp != null)
            {
                Console.WriteLine("");
                Console.WriteLine(bodyRsp);
            }

            Console.WriteLine("");
            Console.WriteLine("");

            return result;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            TestCosmosDbConnection();
        }

        private static void TestCosmosDbConnection()
        {
            var dbHost = "https://localhost:8081";
            var dbKey = "C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==";

            var dbClient = new CosmosClient(dbHost, dbKey,
                new CosmosClientOptions()
                {
                    HttpClientFactory = () => new HttpClient(new CustomHttpHandler(new SocketsHttpHandler())),
                });


            // Check CosmosDB UI available
            //var client = new HttpClient();
            //var get = client.GetAsync("https://localhost:8081/_explorer/index.html").GetAwaiter().GetResult();

            try
            {
                var db = dbClient.CreateDatabaseIfNotExistsAsync("test").GetAwaiter().GetResult();
                Console.WriteLine("db created");
            }
            catch (Exception e)
            {
                Console.WriteLine("error");
            }
        }
    }
}
